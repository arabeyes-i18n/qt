<!DOCTYPE TS><TS>
<!-- Translation achieved by Arabeyes Project (http://www.arabeyes.org) -->
<!-- Translator(s): -->
<!-- Youcef Rabah Rahal <rahal@arabeyes.org> -->
<context>
    <name>AboutDialog</name>
    <message>
        <source>Qt Linguist</source>
        <translation>لغوي Qt</translation>
    </message>
    <message>
        <source>Copyright (C) 2000-2002 Trolltech AS. All Rights Reserved.</source>
        <translation>2002-2000 جميع الحقوق محفوظة لشركة ترولتك.</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program is licensed to you under the terms of the GNU General Public License Version 2 as published by the Free Software Foundation. This gives you legal permission to copy, distribute and/or modify this software under certain conditions. For details, see the file &apos;LICENSE.GPL&apos; that came with this software distribution. If you did not get the file, send email to info@trolltech.com.&lt;/p&gt;
&lt;p&gt;The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.&lt;/p&gt;</source>
        <translation>&lt;p&gt; هذا البرنامج مرخّص لك تحت بنود الرّخصة العامّة الجامعة ل GNU إصدار 2 كما نشرت من طرف مؤسّسة البرامج الحرّة. هذا يسمح لك قانونيابنسخ، توزيع و/أو تعديل هذا البرنامج تحت بعض الشّروط. للتّفاصيل، أنظر إلى الملفّ &apos;LICENSE.GPL&apos; الآتي مع توزيعة البرنامج هذه. في حالة عدم حصولك على هذا الملفّ، إرسل إلى info@trolltech.com.&lt;/p&gt;
&lt;p&gt;هذا البرنامج مورّد في حالته الحالية، دون أيّ شكل من أشكال الضّمان، الّتي تشمل ضمان التّصميم، قابلية التّجارة و الانسجام لهدف خاصّ.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>موافقة</translation>
    </message>
</context>
<context>
    <name>EditorPage</name>
    <message>
        <source>Source text</source>
        <translation>نصّ المصدر</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation>ترجمة</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <comment>Choose Edit|Find from the menu bar or press Ctrl+F to pop up the Find dialog</comment>
    <message>
        <source>Fi&amp;nd what:</source>
        <translation>إي&amp;جاد ماذا:</translation>
    </message>
    <message>
        <source>&amp;Find Next</source>
        <translation>&amp;إيجاد التّالي</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <source>&amp;Match case</source>
        <translation>م&amp;واءمة حالة الأحرف</translation>
    </message>
    <message>
        <source>This window allows you to search and replace some text in the translations.</source>
        <translation>هذه النّافذة تسمح لك بالبحث و استبدال بعض النّصوص في التّرجمات.</translation>
    </message>
    <message>
        <source>Replace &amp;with:</source>
        <translation>استب&amp;دال ب:</translation>
    </message>
    <message>
        <source>&amp;Replace</source>
        <translation>است&amp;بدال</translation>
    </message>
    <message>
        <source>Replace &amp;All</source>
        <translation>استبدال الج&amp;ميع</translation>
    </message>
    <message>
        <source>Click here to replace the next occurrence of the text you typed in.</source>
        <translation>إضغط هنالاستبدال الحدوث التّالي للنّص الّذي أدخلته.</translation>
    </message>
    <message>
        <source>Click here to replace all occurrences of the text you typed in.</source>
        <translation>إضغط هنالاستبدال جميع حدوثات النصّ الّذي أدخلته.</translation>
    </message>
    <message>
        <source>This window allows you to search for some text in the translation source file.</source>
        <translation>هذه النّافذة تسمح لك بالبحث عن بعض النّصوص في ترجمة ملفّ المصدر.</translation>
    </message>
    <message>
        <source>&amp;Source texts</source>
        <translation>نصوص الم&amp;صدر</translation>
    </message>
    <message>
        <source>&amp;Translations</source>
        <translation>&amp;ترجمات</translation>
    </message>
    <message>
        <source>&amp;Comments</source>
        <translation>ت&amp;عاليق</translation>
    </message>
    <message>
        <source>Source texts are searched when checked.</source>
        <translation>نصوص المصدر تبحث في حالة فحصها.</translation>
    </message>
    <message>
        <source>Translations are searched when checked.</source>
        <translation>التّرجمات تبحث في حالة فحصها.</translation>
    </message>
    <message>
        <source>Comments and contexts are searched when checked.</source>
        <translation>التّعاليق و السّياقات تبحث في حالة فحصها.</translation>
    </message>
    <message>
        <source>Type in the text to search for.</source>
        <translation>أدخل النصّ الّذي تودّ البحث عنه.</translation>
    </message>
    <message>
        <source>Texts such as &apos;TeX&apos; and &apos;tex&apos; are considered as different when checked.</source>
        <translation>النّصوص ك &apos;TeX&apos; أو &apos;tex&apos; تعتبر مختلفة حين فحصها.</translation>
    </message>
    <message>
        <source>Click here to find the next occurrence of the text you typed in.</source>
        <translation>إضغط هنالإيجاد الحدوث التّالي للنّص الّذي أدخلته.</translation>
    </message>
    <message>
        <source>Click here to close this window.</source>
        <translation>إضغط هنالإغلاق هذه النّافذة.</translation>
    </message>
</context>
<context>
    <name>MessageEditor</name>
    <message>
        <source>bell</source>
        <translation>جرس</translation>
    </message>
    <message>
        <source>backspace</source>
        <translation>فراغ للوراء</translation>
    </message>
    <message>
        <source>new page</source>
        <translation>صفحة جديدة</translation>
    </message>
    <message>
        <source>new line</source>
        <translation>سطر جديد</translation>
    </message>
    <message>
        <source>carriage return</source>
        <translation>عودة رأس الطباعة</translation>
    </message>
    <message>
        <source>tab</source>
        <translation>جدولة</translation>
    </message>
    <message>
        <source>Source text</source>
        <translation>نصّ المصدر</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>مكمل</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation>ترجمة</translation>
    </message>
    <message>
        <source>Phrases</source>
        <translation>جمل</translation>
    </message>
    <message>
        <source>Phrases and guesses:</source>
        <translation>جمل و حزرات:</translation>
    </message>
    <message>
        <source>This whole panel allows you to view and edit the translation of some source text.</source>
        <translation>مجمل هذه اللّوحة تسمح لك بمعاينة و تحرير ترجمة بعض النّصوص.</translation>
    </message>
    <message>
        <source>This area shows the source text.</source>
        <translation>هذه المنطقة تعرض نصّ المصدر.</translation>
    </message>
    <message>
        <source>This area shows a comment that may guide you, and the context in which the text occurs.</source>
        <translation>هذه المنطقة تعرض تعليقا بإمكانه توجيهك، و السّياق الّذي يحدث فيه النصّ.</translation>
    </message>
    <message>
        <source>This is where you can enter or modify the translation of some source text.</source>
        <translation>هنا يمكنك إدخال أو تعديل ترجمة بعض النّصوص.</translation>
    </message>
    <message>
        <source>Guess</source>
        <translation>حزر</translation>
    </message>
    <message>
        <source>Guess (%1)</source>
        <translation>حزر (%1)</translation>
    </message>
</context>
<context>
    <name>PageCurl</name>
    <message>
        <source>Next unfinished phrase</source>
        <translation>الجملة غير المكملة التّالية</translation>
    </message>
    <message>
        <source>Previous unfinished phrase</source>
        <translation>الجملة غير المكملة السّابقة</translation>
    </message>
</context>
<context>
    <name>PhraseBookBox</name>
    <comment>Go to Phrase &gt; Edit Phrase Book... The dialog that pops up is a PhraseBookBox.</comment>
    <message>
        <source>S&amp;ource phrase:</source>
        <translation>جملة الم&amp;صدر:</translation>
    </message>
    <message>
        <source>&amp;Translation:</source>
        <translation>&amp;ترجمة:</translation>
    </message>
    <message>
        <source>&amp;Definition:</source>
        <translation>ت&amp;عاريف:</translation>
    </message>
    <message>
        <source>&amp;New Phrase</source>
        <translation>&amp;جملة جديدة</translation>
    </message>
    <message>
        <source>&amp;Remove Phrase</source>
        <translation>&amp;نزع الجملة</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;حفظ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <source>This window allows you to add, modify, or delete phrases in a phrase book.</source>
        <translation>هذه النّافذة تسمح لك بإضافة، تعديل أو حذف جمل من كتاب جمل.</translation>
    </message>
    <message>
        <source>This is the phrase in the source language.</source>
        <translation>هذه هي الجملة في لغة المصدر.</translation>
    </message>
    <message>
        <source>This is the phrase in the target language corresponding to the source phrase.</source>
        <translation>هذه هي الجملة في اللّغة المستهدفة و الموافقة لجملة المصدر.</translation>
    </message>
    <message>
        <source>This is a definition for the source phrase.</source>
        <translation>هذا تعريف لجملة المصدر.</translation>
    </message>
    <message>
        <source>Click here to add the phrase to the phrase book.</source>
        <translation>إضغط هنالإضافة الجملة لكتاب الجمل.</translation>
    </message>
    <message>
        <source>Click here to remove the phrase from the phrase book.</source>
        <translation>إضغط هنالنزع الجملة من كتاب الجمل.</translation>
    </message>
    <message>
        <source>Click here to save the changes made.</source>
        <translation>إضغط هنالحفظ التّغييرات.</translation>
    </message>
    <message>
        <source>Click here to close this window.</source>
        <translation>إضغط هنالإغلاق هذه النّافذة.</translation>
    </message>
    <message>
        <source>Qt Linguist</source>
        <translation>لغوي Qt</translation>
    </message>
    <message>
        <source>Cannot save phrase book &apos;%1&apos;.</source>
        <translation>لم أستطع حفظ كتاب الجمل &apos;%1&apos;.</translation>
    </message>
</context>
<context>
    <name>PhraseLV</name>
    <comment>The phrase list in the right panel of the main window (with Source phrase, Target phrase, and Definition in its header) is a PhraseLV object.</comment>
    <message>
        <source>(New Phrase)</source>
        <translation>(جملة جديدة)</translation>
    </message>
    <message>
        <source>This is a list of phrase entries relevant to the source text.  Each phrase is supplemented with a suggested translation and a definition.</source>
        <translation>هذه قائمة الجمل المدخولة المتّصلة بنصّ المصدر. كلّ جملة متمّة بترجمة مقترحة و تعريف.</translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;u&gt;%1:&lt;/u&gt;&amp;nbsp;&amp;nbsp;%2&lt;/p&gt;&lt;p&gt;&lt;u&gt;%3:&lt;/u&gt;&amp;nbsp;&amp;nbsp;%4&lt;/p&gt;&lt;p&gt;&lt;u&gt;%5:&lt;/u&gt;&amp;nbsp;&amp;nbsp;%6&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;u&gt;%1:&lt;/u&gt;&amp;nbsp;&amp;nbsp;%2&lt;/p&gt;&lt;p&gt;&lt;u&gt;%3:&lt;/u&gt;&amp;nbsp;&amp;nbsp;%4&lt;/p&gt;&lt;p&gt;&lt;u&gt;%5:&lt;/u&gt;&amp;nbsp;&amp;nbsp;%6&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Source phrase</source>
        <translation>جملة المصدر</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation>ترجمة</translation>
    </message>
    <message>
        <source>Definition</source>
        <translation>تعريف</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Qt Linguist</source>
        <translation>لغوي Qt</translation>
    </message>
</context>
<context>
    <name>TrWindow</name>
    <comment>This is the application&apos;s main window.</comment>
    <message>
        <source>Context</source>
        <translation>سياق</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>مكمل</translation>
    </message>
    <message>
        <source>Items</source>
        <translation>بنود</translation>
    </message>
    <message>
        <source>MOD</source>
        <translation>معدّل</translation>
    </message>
    <message>
        <source>Qt Linguist</source>
        <translation>لغوي Qt</translation>
    </message>
    <message>
        <source>This panel lists the source contexts.</source>
        <translation>هذه اللّوحة تحصي سياقات المصدر.</translation>
    </message>
    <message>
        <source>This panel lists the source texts. Items that violate validation rules are marked with a warning.</source>
        <translation>هذه اللّوحة تحصي نصوص المصدر. البنود الّتي تخالف قواعد الصّلاحية موسومة بتحذير.</translation>
    </message>
    <message>
        <source>Loading...</source>
        <translation>تحميل...</translation>
    </message>
    <message>
        <source>%1 source phrase(s) loaded.</source>
        <translation> تمّ تحميل %1 جملة مصدر.</translation>
    </message>
    <message>
        <source>Cannot open &apos;%1&apos;.</source>
        <translation>لم أستطع فتح &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Qt translation source (*.ts)
All files (*)</source>
        <translation>مصدر ترجمة Qt (ts.*)
جميع الملفّات (*)</translation>
    </message>
    <message>
        <source>File saved.</source>
        <translation>تمّ حفظ الملفّ.</translation>
    </message>
    <message>
        <source>Cannot save &apos;%1&apos;.</source>
        <translation>لم أستطع حفظ &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Qt message files for released applications (*.qm)
All files (*)</source>
        <translation>ملفّات رسائل Qt للبرامج المصدرة (qm.*)
جميع الملفّات (*)</translation>
    </message>
    <message>
        <source>Release</source>
        <translation>إصدار</translation>
    </message>
    <message>
        <source>File created.</source>
        <translation>تمّ صنع الملفّ.</translation>
    </message>
    <message>
        <source>Printing...</source>
        <translation>طبع...</translation>
    </message>
    <message>
        <source>Context: %1</source>
        <translation>سياق: %1</translation>
    </message>
    <message>
        <source>finished</source>
        <translation>مكمل</translation>
    </message>
    <message>
        <source>unresolved</source>
        <translation>غير مميّز</translation>
    </message>
    <message>
        <source>obsolete</source>
        <translation>مهجور</translation>
    </message>
    <message>
        <source>Printing... (page %1)</source>
        <translation>طبع... (صفحة %1)</translation>
    </message>
    <message>
        <source>Printing completed</source>
        <translation>تمّ الطّبع</translation>
    </message>
    <message>
        <source>Printing aborted</source>
        <translation>أبطل الطّبع</translation>
    </message>
    <message>
        <source>Search wrapped.</source>
        <translation>بحث مطوّق.</translation>
    </message>
    <message>
        <source>Cannot find the string &apos;%1&apos;.</source>
        <translation>لم أستطع إيجاد السّلسلة &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Qt phrase books (*.qph)
All files (*)</source>
        <translation>كتب الجمل Qt (qph.*)
جميع الملفّات (*)</translation>
    </message>
    <message>
        <source>Create New Phrase Book</source>
        <translation>صنع كتاب جمل جديد</translation>
    </message>
    <message>
        <source>A file called &apos;%1&apos; already exists.  Please choose another name.</source>
        <translation>ملفّ بإسم &apos;%1&apos; موجود. إختر إسما آخرا من فضلك.</translation>
    </message>
    <message>
        <source>Phrase book created.</source>
        <translation>تمّ صنع كتاب الجمل.</translation>
    </message>
    <message>
        <source>%1 phrase(s) loaded.</source>
        <translation> تمّ تحميل %1 جملة.</translation>
    </message>
    <message>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <source>Do you want to save &apos;%1&apos;?</source>
        <translation>هل تريد حفظ &apos;%1&apos;؟</translation>
    </message>
    <message>
        <source>Qt Linguist by Trolltech</source>
        <translation>لغوي Qt من ترولتك</translation>
    </message>
    <message>
        <source>No phrase to translate.</source>
        <translation>ليست هناك أيّ جملة للتّرجمة.</translation>
    </message>
    <message>
        <source>No untranslated phrases left.</source>
        <translation>لم تترك أيّ جملة غير مترجمة.</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;ملفّ</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>ت&amp;حرير</translation>
    </message>
    <message>
        <source>&amp;Translation</source>
        <translation>ت&amp;رجمة</translation>
    </message>
    <message>
        <source>V&amp;alidation</source>
        <translation>&amp;صلاحية</translation>
    </message>
    <message>
        <source>&amp;Phrases</source>
        <translation>&amp;جمل</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>م&amp;نظر</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>م&amp;ساعدة</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp;فتح...</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>تحكّم+O</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>حف&amp;ظ</translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation>تحكّم+S</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>حفظ &amp;تحت...</translation>
    </message>
    <message>
        <source>&amp;Release...</source>
        <translation>&amp;إصدار...</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>&amp;طبع...</translation>
    </message>
    <message>
        <source>Ctrl+P</source>
        <translation>تحكّم+P</translation>
    </message>
    <message>
        <source>Re&amp;cently opened files</source>
        <translation>الم&amp;لفّات الّتي فتحت أخيرا</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;خروج</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <translation>تحكّم+Q</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>تر&amp;اجع</translation>
    </message>
    <message>
        <source>Ctrl+Z</source>
        <translation>تحكّم+Z</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>إ&amp;عادة</translation>
    </message>
    <message>
        <source>Ctrl+Y</source>
        <translation>تحكّم+Y</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>&amp;قصّ</translation>
    </message>
    <message>
        <source>Ctrl+X</source>
        <translation>تحكّم+X</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;نسخ</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation>تحكّم+C</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>ت&amp;لصيق</translation>
    </message>
    <message>
        <source>Ctrl+V</source>
        <translation>تحكّم+V</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation>انتقاء الجم&amp;يع</translation>
    </message>
    <message>
        <source>Ctrl+A</source>
        <translation>تحكّم+A</translation>
    </message>
    <message>
        <source>&amp;Find...</source>
        <translation>إيجا&amp;د...</translation>
    </message>
    <message>
        <source>Ctrl+F</source>
        <translation>تحكّم+F</translation>
    </message>
    <message>
        <source>Find &amp;Next</source>
        <translation>إيجاد التّ&amp;الي</translation>
    </message>
    <message>
        <source>&amp;Replace...</source>
        <translation>است&amp;بدال...</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <translation>تحكّم+H</translation>
    </message>
    <message>
        <source>&amp;Prev Unfinished</source>
        <translation>غير المكمل ال&amp;سّابق</translation>
    </message>
    <message>
        <source>Ctrl+K</source>
        <translation>تحكّم+K</translation>
    </message>
    <message>
        <source>&amp;Next Unfinished</source>
        <translation>غير المكمل ال&amp;تّالي</translation>
    </message>
    <message>
        <source>Ctrl+L</source>
        <translation>تحكّم+L</translation>
    </message>
    <message>
        <source>P&amp;rev</source>
        <translation>السّا&amp;بق</translation>
    </message>
    <message>
        <source>Ctrl+Shift+K</source>
        <translation>تحكّم+إزاحة+K</translation>
    </message>
    <message>
        <source>Ne&amp;xt</source>
        <translation>التّا&amp;لي</translation>
    </message>
    <message>
        <source>Ctrl+Shift+L</source>
        <translation>تحكّم+إزاحة+L</translation>
    </message>
    <message>
        <source>Done and &amp;Next</source>
        <translation>مكمل و للتّال&amp;ي</translation>
    </message>
    <message>
        <source>Ctrl+Enter</source>
        <translation>تحكّم+إدخال</translation>
    </message>
    <message>
        <source>Ctrl+Return</source>
        <translation>تحكّم+عودة</translation>
    </message>
    <message>
        <source>&amp;Begin from Source</source>
        <translation>&amp;بدأ من المصدر</translation>
    </message>
    <message>
        <source>Ctrl+B</source>
        <translation>تحكّم+B</translation>
    </message>
    <message>
        <source>&amp;New Phrase Book...</source>
        <translation>كتاب جمل &amp;جديد...</translation>
    </message>
    <message>
        <source>Ctrl+N</source>
        <translation>تحكّم+N</translation>
    </message>
    <message>
        <source>&amp;Open Phrase Book...</source>
        <translation>فت&amp;ح كتاب جمل...</translation>
    </message>
    <message>
        <source>&amp;Close Phrase Book</source>
        <translation>&amp;إغلاق كتاب الجمل</translation>
    </message>
    <message>
        <source>&amp;Edit Phrase Book...</source>
        <translation>&amp;تحرير كتاب الجمل...</translation>
    </message>
    <message>
        <source>&amp;Print Phrase Book...</source>
        <translation>&amp;طبع كتاب الجمل...</translation>
    </message>
    <message>
        <source>&amp;Accelerators</source>
        <translation>&amp;مسرّعات</translation>
    </message>
    <message>
        <source>&amp;Ending Punctuation</source>
        <translation>تنقيط ال&amp;نّهاية</translation>
    </message>
    <message>
        <source>&amp;Phrase Matches</source>
        <translation>ت&amp;واءم الجمل</translation>
    </message>
    <message>
        <source>&amp;Revert Sorting</source>
        <translation>&amp;عكس الفرز</translation>
    </message>
    <message>
        <source>&amp;Display guesses</source>
        <translation>ع&amp;رض الأحزار</translation>
    </message>
    <message>
        <source>Vie&amp;ws</source>
        <translation>منا&amp;ظر</translation>
    </message>
    <message>
        <source>&amp;Toolbars</source>
        <translation>&amp;عيدان الأدوات</translation>
    </message>
    <message>
        <source>&amp;Manual</source>
        <translation>&amp;دليل</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;حول</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>ح&amp;ول Qt</translation>
    </message>
    <message>
        <source>&amp;What&apos;s This?</source>
        <translation>ما &amp;هذا؟</translation>
    </message>
    <message>
        <source>Open a Qt translation source file (TS file) for editing.</source>
        <translation>فتح ملفّ مصدر ترجمة Qt (ملفّ TS) للتّحرير.</translation>
    </message>
    <message>
        <source>Save changes made to this Qt translation source file.</source>
        <translation>حفظ التّعديلات المجراة لملفّ مصدر ترجمة Qt هذا.</translation>
    </message>
    <message>
        <source>Save changes made to this Qt translationsource file into a new file.</source>
        <translation>حفظ التّعديلات المجراة لملفّ مصدر ترجمة Qt هذا في ملفّ جديد.</translation>
    </message>
    <message>
        <source>Create a Qt message file suitable for released applications from the current message file.</source>
        <translation>صنع ملفّ رسائل Qt مناسب للبرامج المصدرة ابتداء من الملفّ الجاري.</translation>
    </message>
    <message>
        <source>Print a list of all the phrases in the current Qt translation source file.</source>
        <translation>طبع قائمة لكلّ الجمل في ملفّ مصدر التّرجمة Qt الجاري.</translation>
    </message>
    <message>
        <source>Close this window and exit.</source>
        <translation>إغلاق هذه النّافذة و الخروج.</translation>
    </message>
    <message>
        <source>Undo the last editing operation performed on the translation.</source>
        <translation>التّراجع عن آخر عمليّة تحرير أجريت على التّرجمة.</translation>
    </message>
    <message>
        <source>Redo an undone editing operation performed on the translation.</source>
        <translation>إعادة عمليّة تحرير أجريت على التّرجمة تمّ التّراجع عنها.</translation>
    </message>
    <message>
        <source>Copy the selected translation text to the clipboard and deletes it.</source>
        <translation>نسخ نصّ التّرجمة المنتقى نحو السّبّورة و حذفه.</translation>
    </message>
    <message>
        <source>Copy the selected translation text to the clipboard.</source>
        <translation>نسخ نصّ التّرجمة المنتقى نحو السّبّورة.</translation>
    </message>
    <message>
        <source>Paste the clipboard text into the translation.</source>
        <translation>تلصيق نصّ السّبّورة نحو التّرجمة.</translation>
    </message>
    <message>
        <source>Select the whole translation text.</source>
        <translation>انتقاء مجمل نصّ التّرجمة.</translation>
    </message>
    <message>
        <source>Search for some text in the translation source file.</source>
        <translation>البحث عن نصّ في ملفّ مصدر التّرجمة.</translation>
    </message>
    <message>
        <source>Continue the search where it was left.</source>
        <translation>مواصلة البحث أين تمّ تركه.</translation>
    </message>
    <message>
        <source>Search for some text in the translation source file and replace it by another text.</source>
        <translation>البحث عن نصّ في ملفّ مصدر التّرجمة واستبداله بنصّ آخر.</translation>
    </message>
    <message>
        <source>Create a new phrase book.</source>
        <translation>صنع كتاب جمل جديد.</translation>
    </message>
    <message>
        <source>Open a phrase book to assist translation.</source>
        <translation>فتح كتاب جمل لإعانة التّرجمة.</translation>
    </message>
    <message>
        <source>Toggle validity checks of accelerators.</source>
        <translation>قلب فحوص صلاحية المسرّعات.</translation>
    </message>
    <message>
        <source>Toggle validity checks of ending punctuation.</source>
        <translation>قلب فحوص التّنقيط النّهائي.</translation>
    </message>
    <message>
        <source>Toggle checking that phrase suggestions are used.</source>
        <translation>قلب الفحص بأنّ اقتراحات الجمل مستعملة.</translation>
    </message>
    <message>
        <source>Sort the items back in the same order as in the message file.</source>
        <translation>إعادة فرز الجمل بنفس ترتيب ملفّ الرّسالة.</translation>
    </message>
    <message>
        <source>Set whether or not to display translation guesses.</source>
        <translation>إعداد عرض أو عدم عرض أحزار التّرجمة.</translation>
    </message>
    <message>
        <source>Display the manual for %1.</source>
        <translation>عرض الدّليل ل %1.</translation>
    </message>
    <message>
        <source>Display information about %1.</source>
        <translation>عرض المعلومات حول %1.</translation>
    </message>
    <message>
        <source>Display information about the Qt toolkit by Trolltech.</source>
        <translation>عرض المعلومات حول مجموعة الأدوات Qt لترولتك.</translation>
    </message>
    <message>
        <source>Enter What&apos;s This? mode.</source>
        <translation>دخول نمط ما هذا؟.</translation>
    </message>
    <message>
        <source>Copies the source text into the translation field.</source>
        <translation>نسخ نصّ المصدر نحو حقل التّرجمة.</translation>
    </message>
    <message>
        <source>Moves to the next item.</source>
        <translation>تحرّك إلى البند التّالي.</translation>
    </message>
    <message>
        <source>Moves to the previous item.</source>
        <translation>تحرّك إلى البند السّابق.</translation>
    </message>
    <message>
        <source>Moves to the next unfinished item.</source>
        <translation>تحرّك إلى البند غير المكمل التّالي.</translation>
    </message>
    <message>
        <source>Moves to the previous unfinished item.</source>
        <translation>تحرّك إلى البند غير المكمل السّابق.</translation>
    </message>
    <message>
        <source>Marks this item as done and moves to the next unfinished item.</source>
        <translation>وسم هذا البند كبند مكمل و التّحرك إلى البند غير المكمل التّالي.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>ملفّ</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>تحرير</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation>ترجمة</translation>
    </message>
    <message>
        <source>Validation</source>
        <translation>صلاحية</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>فتح</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>طبع</translation>
    </message>
    <message>
        <source>Open Phrase Book</source>
        <translation>فتح كتاب الجمل</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>تراجع</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>إعادة</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>قصّ</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>نسخ</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>تلصيق</translation>
    </message>
    <message>
        <source>Find</source>
        <translation>إيجاد</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>استبدال</translation>
    </message>
    <message>
        <source>Prev</source>
        <translation>السّابق</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>التّالي</translation>
    </message>
    <message>
        <source>Prev Unfinished</source>
        <translation>غير المكمل السّابق</translation>
    </message>
    <message>
        <source>Next Unfinished</source>
        <translation>غير المكمل التّالي</translation>
    </message>
    <message>
        <source>Done and Next</source>
        <translation>مكمل و للتّالي</translation>
    </message>
    <message>
        <source>Accelerators</source>
        <translation>مسرّعات</translation>
    </message>
    <message>
        <source>Punctuation</source>
        <translation>تنقيط</translation>
    </message>
    <message>
        <source>Phrases</source>
        <translation>جمل</translation>
    </message>
    <message>
        <source>What&apos;s This?</source>
        <translation>ما هذا؟</translation>
    </message>
    <message>
        <source>Cannot read from phrase book &apos;%1&apos;.</source>
        <translation>لم أستطع القراءة من كتاب الجمل &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Close this phrase book.</source>
        <translation>إغلاق كتاب الجمل هذا.</translation>
    </message>
    <message>
        <source>Allow you to add, modify, or delete phrases of this phrase book.</source>
        <translation>يسمح لك بإضافة، تعديل أو حذف جمل من كتاب الجمل هذا.</translation>
    </message>
    <message>
        <source>Print the entries of the phrase book.</source>
        <translation>طبع تدوينات كتاب الجمل.</translation>
    </message>
    <message>
        <source>Cannot create phrase book &apos;%1&apos;.</source>
        <translation>لم أستطع صنع كتاب الجمل &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>Accelerator possibly superfluous in translation.</source>
        <translation>ربّما هناك مسرّع نافل.</translation>
    </message>
    <message>
        <source>Accelerator possibly missing in translation.</source>
        <translation>ربّما هناك مسرّع ناقص.</translation>
    </message>
    <message>
        <source>Translation does not end with the same punctuation as the source text.</source>
        <translation>التّرجمة لا تنتهي بنفس التّنقيط كنصّ المصدر.</translation>
    </message>
    <message>
        <source>A phrase book suggestion for &apos;%1&apos; was ignored.</source>
        <translation>تمّ تجاهل اقتراح كتاب الجمل فيما يخصّ &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>إصدار %1</translation>
    </message>
</context>
</TS>
