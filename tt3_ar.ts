<!DOCTYPE TS><TS>
<!-- Translation achieved by Arabeyes Project (http://www.arabeyes.org) -->
<!-- Translator(s): -->
<!-- Youcef Rabah Rahal <rahal@arabeyes.org> -->
<context>
    <name>MainWindow</name>
    <message>
        <source>Troll Print 1.0</source>
        <translation>طبع ترول 1.0</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;خروج</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;حول</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>ح&amp;ول Qt</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;ملفّ</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>م&amp;ساعدة</translation>
    </message>
    <message>
        <source>About Troll Print 1.0</source>
        <translation>حول طبع ترول 1.0</translation>
    </message>
    <message>
        <source>Troll Print 1.0.

Copyright 1999 Macroshaft, Inc.</source>
        <translation>طبع ترول 1.0
شركة ماكروشافت، جميع الحقوق محفوظة 1999.</translation>
    </message>
    <message>
        <source>Ctrl+Q</source>
        <comment>Quit</comment>
        <translation>تحكّم+Q</translation>
    </message>
</context>
<context>
    <name>PrintPanel</name>
    <message>
        <source>2-sided</source>
        <translation>من الجانبين</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>ممكّن</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>معجّز</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation>الألوان</translation>
    </message>
</context>
</TS>
