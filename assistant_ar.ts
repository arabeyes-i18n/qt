<!DOCTYPE TS><TS>
<!-- Translation achieved by Arabeyes Project (http://www.arabeyes.org) -->
<!-- Translator(s): -->
<!-- Youcef Rabah Rahal <rahal@arabeyes.org> -->
<context>
    <name>Assistant</name>
    <message>
        <source>Welcome to the &lt;b&gt;Qt Assistant&lt;/b&gt;. Qt Assistant will give you quicker access to help and tips while using applications like Qt Designer.</source>
        <translation>مرحبا في &lt;b&gt;معين Qt&lt;/b&gt;. معين Qt سيعطيك توصّلا أسرع للمساعدة و للإشارات أثناء استعمالك البرامج كمصمّم Qt.</translation>
    </message>
    <message>
        <source>Qt Assistant</source>
        <translation>معين Qt</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <source>Find Text</source>
        <translation>إيجاد النّص</translation>
    </message>
    <message>
        <source>&amp;Find:</source>
        <translation>&amp;إيجاد:</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>إ&amp;يجاد</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <source>&amp;Direction</source>
        <translation>إ&amp;تّجاه</translation>
    </message>
    <message>
        <source>Forwar&amp;d</source>
        <translation>لل&amp;أمام</translation>
    </message>
    <message>
        <source>&amp;Backward</source>
        <translation>لل&amp;وراء</translation>
    </message>
    <message>
        <source>&amp;Options</source>
        <translation>&amp;خيارات</translation>
    </message>
    <message>
        <source>&amp;Whole words only</source>
        <translation>الكلمات ال&amp;كاملة فقط</translation>
    </message>
    <message>
        <source>&amp;Case sensitive</source>
        <translation>&amp;حساسية لحالة الأحرف</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <source>Index</source>
        <translation>فهرس</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>مؤشّرات</translation>
    </message>
    <message>
        <source>Con&amp;tents</source>
        <translation>م&amp;حتويات</translation>
    </message>
    <message>
        <source>Qt Reference Documentation</source>
        <translation>توتيق Qt المرجعي</translation>
    </message>
    <message>
        <source>Qt Designer Manual</source>
        <translation>دليل مصمّم Qt</translation>
    </message>
    <message>
        <source>Qt Linguist Manual</source>
        <translation>دليل لغوي Qt</translation>
    </message>
    <message>
        <source>Qt Assistant Manual</source>
        <translation>دليل معين Qt</translation>
    </message>
    <message>
        <source>Qt Assistant</source>
        <translation>معين Qt</translation>
    </message>
</context>
<context>
    <name>HelpDialogBase</name>
    <message>
        <source>Help</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <source>&lt;b&gt;Help&lt;/b&gt;&lt;p&gt;Choose the topic you need help for from the contents list, or search the index for keywords.&lt;/p&gt;</source>
        <translation>&lt;b&gt;مساعدة&lt;/b&gt;&lt;p&gt;إختر الموضوع الّذي تريد عليه المساعدة من قائمة المحتويات، أو إبحث في الفهرس عن كلمات الدّلالة.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Displays help topics organized by category, index or bookmarks</source>
        <translation>يعرض مواضيع المساعدة منظّمة حسب الطّائفة، الفهرس أو المؤشّرات</translation>
    </message>
    <message>
        <source>Con&amp;tents</source>
        <translation>م&amp;حتويات</translation>
    </message>
    <message>
        <source>Column 1</source>
        <translation>عمود 1</translation>
    </message>
    <message>
        <source>&lt;b&gt;Help topics organized by category.&lt;/b&gt;&lt;p&gt;Double-click an item to see which topics are in that category. To view a topic, select it, and then click &lt;b&gt;Display&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;b&gt;مواضيع المساعدة منظّمة حسب الطّائفة.&lt;/b&gt;&lt;p&gt;إضغط مرّتين على بند لرؤية المواضيع المتواجدة في تلك الطّائفة. للنّظر إلى موضوع، انتقيه، ثمّ إضغط على &lt;b&gt;عرض&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Index</source>
        <translation>&amp;فهرس</translation>
    </message>
    <message>
        <source>&amp;Look For:</source>
        <translation>&amp;بحث عن:</translation>
    </message>
    <message>
        <source>Enter keyword</source>
        <translation>إدخال كلمة الدّلالة</translation>
    </message>
    <message>
        <source>&lt;b&gt;Enter a keyword.&lt;/b&gt;&lt;p&gt;The list will select an item that matches the entered string best.&lt;/p&gt;</source>
        <translation>&lt;b&gt;أدخل كلمة دلالة.&lt;/b&gt;&lt;p&gt;ستنتقي القائمة  أفضل بند مطابق للسّلسلة المدخولة.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;List of available help topics.&lt;/b&gt;&lt;p&gt;Double-click on an item to open up the help page for this topic. You will have to choose the right page if more than one are found.&lt;/p&gt;</source>
        <translation>&lt;b&gt;قائمة مواضيع المساعدة المتوفّرة.&lt;/b&gt;&lt;p&gt;إضغط مرّتين على بند لفتح صفحة المساعدة لهذا الموضوع. يتوجّب اختيار الصّفحة الصّواب في حالة إيجاد أكثر من صفحة واحدة.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&amp;Bookmarks</source>
        <translation>&amp;مؤشّرات</translation>
    </message>
    <message>
        <source>Displays the list of bookmarks.</source>
        <translation>عرض قائمة المؤشّرات.</translation>
    </message>
    <message>
        <source>&amp;New Bookmark</source>
        <translation>مؤشّر &amp;جديد</translation>
    </message>
    <message>
        <source>Add new bookmark</source>
        <translation>إضافة مؤشّر جديد</translation>
    </message>
    <message>
        <source>Add the current displayed page as new bookmark to the list.</source>
        <translation>إضافة الصّفحة المعروضة حاليا كمؤشّر جديد في القائمة.</translation>
    </message>
    <message>
        <source>D&amp;elete Bookmark</source>
        <translation>ح&amp;ذف مؤشّر</translation>
    </message>
    <message>
        <source>Delete bookmark</source>
        <translation>حذف مؤشّر</translation>
    </message>
    <message>
        <source>Delete the selected bookmark from the list.</source>
        <translation>حذف المؤشّر المنتقى من القائمة.</translation>
    </message>
    <message>
        <source>Preparing...</source>
        <translation>تحضير...</translation>
    </message>
</context>
<context>
    <name>HelpWindow</name>
    <message>
        <source>Qt Assistant by Trolltech - %1</source>
        <translation>معين Qt من طرف ترولتك - %1</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>مساعدة</translation>
    </message>
    <message>
        <source>Can&apos;t load and display non-local file
%1</source>
        <translation>لم أستطع تحميل و عرض الملفّ غير المحلّي
%1</translation>
    </message>
    <message>
        <source>Open Link in New Window<byte value="x9"/>Shift+LMB</source>
        <translation>فتح الوصل في نافذة جديدة	إزاحة+LMB</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Qt Assistant by Trolltech</source>
        <translation>معين Qt من طرف ترولتك</translation>
    </message>
    <message>
        <source>Toolbar</source>
        <translation>عود الأدوات</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>طبع</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>&amp;طبع...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>خروج</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;خروج</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>نسخ</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;نسخ</translation>
    </message>
    <message>
        <source>Find in Text</source>
        <translation>إيجاد في النّص</translation>
    </message>
    <message>
        <source>&amp;Find in Text...</source>
        <translation>&amp;إيجاد في النّص...</translation>
    </message>
    <message>
        <source>Home</source>
        <translation>منزل</translation>
    </message>
    <message>
        <source>&amp;Home</source>
        <translation>&amp;منزل</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>السّابق</translation>
    </message>
    <message>
        <source>&amp;Previous</source>
        <translation>ال&amp;سّابق</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>التّالي</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>ال&amp;تّالي</translation>
    </message>
    <message>
        <source>About</source>
        <translation>حول</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>حول Qt</translation>
    </message>
    <message>
        <source>Qt Class Reference</source>
        <translation>مرجع فصائل Qt</translation>
    </message>
    <message>
        <source>Qt Designer Manual</source>
        <translation>دليل مصمّم Qt</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation>تكبير للدّاخل</translation>
    </message>
    <message>
        <source>Zoom &amp;in</source>
        <translation>تكبير لل&amp;دّاخل</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>تكبير للخارج</translation>
    </message>
    <message>
        <source>Zoom &amp;out</source>
        <translation>تكبير للخا&amp;رج</translation>
    </message>
    <message>
        <source>Qt Linguist Manual</source>
        <translation>دليل لغوي Qt</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>إ&amp;عدادات</translation>
    </message>
    <message>
        <source>&amp;Settings...</source>
        <translation>إ&amp;عدادات...</translation>
    </message>
    <message>
        <source>New Window</source>
        <translation>نافذة جديدة</translation>
    </message>
    <message>
        <source>New Window...</source>
        <translation>نافذة جديدة...</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>إغلاق</translation>
    </message>
    <message>
        <source>Vie&amp;ws</source>
        <translation>منا&amp;ظر</translation>
    </message>
    <message>
        <source>Ctrl+T</source>
        <translation>تحكّم+T</translation>
    </message>
    <message>
        <source>Ctrl+I</source>
        <translation>تحكّم+I</translation>
    </message>
    <message>
        <source>Ctrl+B</source>
        <translation>تحكّم+B</translation>
    </message>
    <message>
        <source>Qt Assistant</source>
        <translation>معين Qt</translation>
    </message>
    <message>
        <source>&amp;Add Bookmark</source>
        <translation>إ&amp;ضافة مؤشّر</translation>
    </message>
    <message>
        <source>Qt Reference Documentation</source>
        <translation>توتيق Qt المرجعي</translation>
    </message>
</context>
<context>
    <name>SettingsDialogBase</name>
    <message>
        <source>Settings</source>
        <translation>إعدادات</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation>خطّ:</translation>
    </message>
    <message>
        <source>Link color:</source>
        <translation>لون الوصل:</translation>
    </message>
    <message>
        <source>Underline links</source>
        <translation>تسطير الوصلات</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>موافقة</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <source>Fixed font:</source>
        <translation>خطّ ثابت:</translation>
    </message>
</context>
<context>
    <name>TopicChooser</name>
    <message>
        <source>Choose a topic for &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>اختيار موضوع ل&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>TopicChooserBase</name>
    <message>
        <source>Choose Topic</source>
        <translation>اختيار موضوع</translation>
    </message>
    <message>
        <source>Select a topic from the list and click the &lt;b&gt;Display&lt;/b&gt;-button to open the online help.</source>
        <translation>إنتقي موضوعا من القائمة و إضغط على الزّر &lt;b&gt;عرض&lt;/b&gt; لفتح المساعدة المباشرة.</translation>
    </message>
    <message>
        <source>&amp;Topics</source>
        <translation>&amp;مواضيع</translation>
    </message>
    <message>
        <source>Displays a list of available help topics for the keyword.</source>
        <translation>عرض قائمة لمواضيع المساعدة المتوفّرة لكلمة الدّلالة.</translation>
    </message>
    <message>
        <source>&amp;Display</source>
        <translation>&amp;عرض</translation>
    </message>
    <message>
        <source>Open the topic selected in the list.</source>
        <translation>فتح الموضوع المنتقى في القائمة.</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;إغلاق</translation>
    </message>
    <message>
        <source>Close the Dialog.</source>
        <translation>إغلاق الحوار.</translation>
    </message>
</context>
</TS>
